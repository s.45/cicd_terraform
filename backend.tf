terraform {
  required_version = ">=0.12.0"
  backend "s3" {
    region  = "us-east-1"
    profile = "aws "
    key     = "my-key-pair"
    bucket  = "terra123456"
  }
}
